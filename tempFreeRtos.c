//*****************************************************************************
//
// freertos_demo.c - Simple FreeRTOS example.
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.0.12573 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include "ENGG4810.h"

//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>FreeRTOS Example (freertos_demo)</h1>
//!
//! This application demonstrates the use of FreeRTOS on Launchpad.
//!
//! The application blinks the user-selected LED at a user-selected frequency.
//! To select the LED press the left button and to select the frequency press
//! the right button.  The UART outputs the application status at 115,200 baud,
//! 8-n-1 mode.
//!
//! This application utilizes FreeRTOS to perform the tasks in a concurrent
//! fashion.  The following tasks are created:
//!
//! - An LED task, which blinks the user-selected on-board LED at a
//!   user-selected rate (changed via the buttons).
//!
//! - A Switch task, which monitors the buttons pressed and passes the
//!   information to LED task.
//!
//! In addition to the tasks, this application also uses the following FreeRTOS
//! resources:
//!
//! - A Queue to enable information transfer between tasks.
//!
//! - A Semaphore to guard the resource, UART, from access by multiple tasks at
//!   the same time.
//!
//! - A non-blocking FreeRTOS Delay to put the tasks in blocked state when they
//!   have nothing to do.
//!
//! For additional details on FreeRTOS, refer to the FreeRTOS web page at:
//! http://www.freertos.org/
//
//*****************************************************************************

//define accelerometer registers and values
#define ACCEL_SLAVE_ADDR 0x53
#define ACCEL_DATA_REG 0x32
#define DATA_FORMAT_REG 0x31
//*****************************************************************************
// Declarations
//*****************************************************************************
void vFATFS_Timer_Task(void* pvParameters);
void vSD_Task(void);
void vAccelerometer_Task(void *pvParameters);
void vGPS_Task(void *pvParameters);
void vTemp_Task(void *pvParameters);

//*****************************************************************************
// The stack sizes for tasks.
//*****************************************************************************
#define mainSD_TASK_STACK					( configMINIMAL_STACK_SIZE * 3 )
#define mainFATFS_TASK_STACK				( configMINIMAL_STACK_SIZE * 3 )
#define mainACCELEROMETER_STACK				( configMINIMAL_STACK_SIZE * 3 )
#define mainGPS_STACK						( configMINIMAL_STACK_SIZE * 3 )
#define mainTEMPERATURE_STACK				( configMINIMAL_STACK_SIZE * 3 )

//#define PRIORITY_ACCELEROMETER				1
#define PRIORITY_TEMPERATURE 					1
#define PRIORITY_GPS							1

//*****************************************************************************
// The item size and queue size for the LED message queue.
//*****************************************************************************

#define SD_ITEM_SIZE           sizeof(uint8_t)
#define SD_QUEUE_SIZE          5

//*****************************************************************************
// The following are data structures used by FatFs.
//*****************************************************************************

static FATFS g_sFatFs;
static DIR g_sDirObject;
static FILINFO g_sFileInfo;
static FIL g_sFileObject;

//*****************************************************************************
// The mutex that protects concurrent access of UART from multiple tasks.
//*****************************************************************************

xSemaphoreHandle g_pUARTSemaphore;
//Queue handles
xQueueHandle xSdDataQueue;
//*****************************************************************************
// The error routine that is called if the driver library encounters an error.
//*****************************************************************************

#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}

#endif



void sendNMEAPkt(){
	int i;

	const char pollNMEA[13] = {'$','C','C','G','P','Q',',','R','M','C', 0x36,'\r','\n'};

	for(i=0;i<13;i++){
		UARTCharPut(UART1_BASE,pollNMEA[i]);
	}
	SysCtlDelay(500000);
}

int recieveNMEAPkt(char * dataBuf){

    char temp_char;
    int Rx_cntr = 0;
    int UARTEnded = 0;
    int NMEA = 0;
    char startStr[7] = {0,0,0,0,0,0,0};




    while(UARTEnded==0){
		temp_char = UARTCharGet(UART1_BASE);


		if(NMEA==0){
			startStr[0] = startStr[1];
			startStr[1] = startStr[2];
			startStr[2] = startStr[3];
			startStr[3] = startStr[4];
			startStr[4] = startStr[5];
			startStr[5] = startStr[6];
			startStr[6] = temp_char;

			if(!strcmp(startStr, "$GPRMC,")){
				NMEA=1;
			}
		}else{

			dataBuf[Rx_cntr] = temp_char;
			Rx_cntr++;
		}




		if(temp_char=='\n'){
			UARTEnded=1;
			dataBuf[Rx_cntr]= 0;
			Rx_cntr++;
		}
    }

    return Rx_cntr;

}
//*****************************************************************************
// This hook is called by FreeRTOS when an stack overflow error is detected.
//*****************************************************************************

void vApplicationStackOverflowHook(xTaskHandle *pxTask, char *pcTaskName) {

    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.

    while(1) {}
}

void ConfigureADC(void){
	  //enable floating point integers
	    ROM_FPULazyStackingEnable();
	    ROM_FPUEnable();
	    //set up the system clock to run at 40MHz
	    ROM_SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

	    //enable the ADC peripheral ADC0
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	    //enable gpio port E
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	    //Set the internal reference (3V)
	    ROM_ADCReferenceSet(ADC0_BASE,ADC_REF_INT);

	    //configure the GPIO port E, to port 3
	    ROM_GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_3);

	    //disable ADC prior to use to make use accurate (due to previous initializations)
	    ROM_ADCSequenceDisable(ADC0_BASE,1);

	    //trigger the ADC sequencer
	    ROM_ADCSequenceConfigure(ADC0_BASE,1,ADC_TRIGGER_PROCESSOR,0);


	    //set the interrupt flag when sampling is done and read from AIN0
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,0,ADC_CTL_CH0);
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,1,ADC_CTL_CH0);
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,2,ADC_CTL_CH0);
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_CH0|ADC_CTL_IE|ADC_CTL_END);

	    //now enable 3rd sequencer
	    ROM_ADCSequenceEnable(ADC0_BASE,1);
}

void ConfigI2C2(void){
	//
	    // The I2C2 peripheral must be enabled before use.
	    //
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

	    //
	    // I2C2 is used with PortE[5:4].
	    //
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

	    //
	    // Configure the pin muxing for I2C2 functions on port E4 and E5.
	    // This step is not necessary if your part does not support pin muxing
	    //
	    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
	    GPIOPinConfigure(GPIO_PB3_I2C0SDA);

	    //
	    // Select the I2C function for these pins.  This function will also
	    // configure the GPIO pins pins for I2C operation, setting them to
	    // open-drain operation with weak pull-ups.
	    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
	    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

	    //
	    // Enable and initialize the I2C2 master module.  Use the system clock for
	    // the I2C2 module.  The last parameter sets the I2C data transfer rate.
	    // If false the data rate is set to 100kbps and if true the data rate will
	    // be set to 400kbps.  For this example we will use a data rate of 100kbps.
	    //
	    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false);

}
//*****************************************************************************
// Configure the UART and its pins.  This must be called before UARTprintf().
//*****************************************************************************

void ConfigureUART(void) {
	 //
	    // Enable the GPIO Peripheral used by the UART.
	    //
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); //usb communication
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); //GPS communication
	    //
	    // Enable UART0 and UART1
	    //
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
	    //
	    // Configure GPIO Pins for UART mode.
	    //
	    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	    ROM_GPIOPinConfigure(GPIO_PC4_U1RX);
	    ROM_GPIOPinConfigure(GPIO_PC5_U1TX);
	    ROM_GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);
	    //
	    // Use the internal 16MHz oscillator as the UART clock source.
	    //
	    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
	    UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);

	    //
	    // Initialize the UART for console I/O.
	    //
	    UARTStdioConfig(0, 115200, 16000000);
	    UARTConfigSetExpClk(UART1_BASE, 16000000, 38400, UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE);//115200

}

//*****************************************************************************
// Initialise FreeRTOS and start the initial set of tasks.
//*****************************************************************************
int main(void) {

    f_mount(0, &g_sFatFs);

    // Set the clocking to run at 50 MHz from the PLL.
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);

    // Enable the peripherals used by this example.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);

    // Initialise the UART and configure it for 115,200, 8-N-1 operation.
    ConfigureUART();
    ConfigI2C2();
    ConfigureADC();
    // Create a mutex to guard the UART.
    g_pUARTSemaphore = xSemaphoreCreateMutex();

    xSdDataQueue = xQueueCreate(1, 32 * sizeof(char));

    // Create tasks
    //xTaskCreate(vSD_Task, (signed portCHAR *)"SD", mainSD_TASK_STACK, NULL,
    	//	PRIORITY_SD_TASK, NULL);

    //xTaskCreate(vFATFS_Timer_Task, (signed portCHAR *)"FATFS", mainFATFS_TASK_STACK, NULL,
    	//	PRIORITY_FATFS_TASK, NULL);

    //xTaskCreate(vAccelerometer_Task, (signed portCHAR *)"Accelerometer", mainACCELEROMETER_STACK, NULL,
    	//	PRIORITY_ACCELEROMETER, NULL);

    //xTaskCreate(vGPS_Task, (signed portCHAR *)"GPS", mainGPS_STACK, NULL,
    	//	PRIORITY_GPS, NULL);
    xTaskCreate(vTemp_Task, (signed portCHAR*)"Temperature", mainTEMPERATURE_STACK,NULL,PRIORITY_TEMPERATURE, NULL);
    // Create the LED task.
    if(LEDTaskInit() != 0) {
        while(1) {}
    }

    // Create the switch task.
    if(SwitchTaskInit() != 0) {
        while(1) {}
    }

    // Start the scheduler.  This should not return.
    vTaskStartScheduler();

    // In case the scheduler returns for some reason, print an error and return 0.
    return 0;
}

//*****************************************************************************
// Tasks
//*****************************************************************************

// FATFS Timer Task - SD card requires clock every 10ms
void vFATFS_Timer_Task(void* pvParameters) {
    portTickType xLastWakeTime = xTaskGetTickCount();

    for(;;) {
        disk_timerproc();
        vTaskDelayUntil(&xLastWakeTime, (10/portTICK_RATE_MS));
    }
}

// SD Task - Write to SD card
void vSD_Task(void) {

    FRESULT iFResult;
    char cData[64];
    char cNewFileName[64];
    int logVersion = 1;

    iFResult = f_opendir(&g_sDirObject, "");
    if (iFResult) die(iFResult);

    // Check file names
	for(;;){
		iFResult = f_readdir(&g_sDirObject, &g_sFileInfo); 					//Read directory item
		if (iFResult || !g_sFileInfo.fname[0]) break;					// Error or end of dir
		if (!(g_sFileInfo.fattrib & AM_DIR)){ 					// Check if not directory
//			if (strstr(g_sFileInfo.fname, "T8_Log") != NULL){
//				logVersion++;
//			}
			logVersion++;
		}
	}

	sprintf(cNewFileName, "T8_Log%d.txt", logVersion);

    // Max filename length is 12 including extension. For longer, modify ffconf.h.
    iFResult = f_open(&g_sFileObject, cNewFileName, FA_WRITE | FA_CREATE_ALWAYS);
    if (iFResult) die(iFResult);
    iFResult = f_sync(&g_sFileObject);
    if (iFResult) die(iFResult);

    for (;;) {
    	if (xQueueReceive(xSdDataQueue, &cData, 500) == pdTRUE) {
			//Write data to file.
    		iFResult = f_printf(&g_sFileObject, "%s\r\n", cData);
    		iFResult = f_sync(&g_sFileObject);
			if (iFResult) die(iFResult);
		}
        vTaskDelay(50);
    }
}

void vAccelerometer_Task(void *pvParameters) {
	char* accData = "test4";
	uint16_t allData[6];
	int16_t XData;
	int16_t YData;
	int16_t ZData;
	int regNum = 0;

	for(;;) {
		//initialise data buffers
		for(regNum = 0; regNum < 6; regNum++){
			allData[regNum] = 0;
		}

		//set master to send and delay to ensure it is set
		I2CMasterSlaveAddrSet(I2C0_BASE,ACCEL_SLAVE_ADDR,false);

		I2CMasterDataPut(I2C0_BASE, DATA_FORMAT_REG);
		I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C0_BASE)){};

		I2CMasterDataPut(I2C0_BASE, 0x2B);
		I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C0_BASE)){};

		I2CMasterSlaveAddrSet(I2C0_BASE,ACCEL_SLAVE_ADDR,false);

		I2CMasterDataPut(I2C0_BASE, 0x2D);
		I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C0_BASE)){};

		I2CMasterDataPut(I2C0_BASE, 0x08);
		I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C0_BASE)){};
		//set to full resolution and +/-16G measurement

		//send write address and data register to read from into buffers and send to Accelerometer
		I2CMasterSlaveAddrSet(I2C0_BASE,ACCEL_SLAVE_ADDR,false);

		I2CMasterDataPut(I2C0_BASE,  ACCEL_DATA_REG);
		I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_SEND);
		while(I2CMasterBusy(I2C0_BASE)); //wait until the master has finished operation

		//Change to read address into buffer and delay to ensure it has set
		I2CMasterSlaveAddrSet(I2C0_BASE,ACCEL_SLAVE_ADDR,true);

		//Receive multiple bytes from the accelerometer
		for(regNum = 0; regNum<6;regNum++){
			//use the appropriate master control function
			if(regNum ==0){ //starting packet, register 1
				I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
			}else if(regNum==5){ //ending packet, register 6
				I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
			}else{// registers 2 to 5 inclusive
				I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			}
			while(I2CMasterBusy(I2C0_BASE));

			allData[regNum] = I2CMasterDataGet(I2C0_BASE);
			//UARTprintf("%d \n", allData[regNum]);

		}
		XData = ((allData[1]<<8)|allData[0]);
		YData = ((allData[3]<<8)|allData[2]);
		ZData = ((allData[5]<<8)|allData[4]);

    	UARTprintf("X:%d, Y:%d, Z:%d \n",XData, YData, ZData);


		xQueueSendToBack(xSdDataQueue, accData, 0);
		vTaskDelay(1000);
	}
}

void vGPS_Task(void *pvParameters) {
		const uint8_t GPSon[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4C, 0x37};
		const uint8_t GPSoff[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4D, 0x3B};
		const uint8_t GPSMaxPower[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x00, 0x21, 0x91};
		const uint8_t GPSEcoMode[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x04, 0x25, 0x95};
		const uint8_t GPSPowSave[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x01, 0x22, 0x92};

		int i;
		uint8_t dataBuff[80];
	    char dataValid = 'V'; //V: invalid , A: Valid
	    int commas = 0;
	    int cntr=0;

		for(i=0;i<10;i++){
			UARTCharPut(UART1_BASE, GPSPowSave[i]);
		}

	for(;;) {

    	dataValid = 'V';
    	while(dataValid != 'A'){
			sendNMEAPkt();
			cntr = recieveNMEAPkt((char *)dataBuff);

			i=0;
			commas=0;
			//see if the data is valid or not
			while(dataBuff[i] != 0){
				if(dataBuff[i]==','){
					commas++;
				}

				if(commas==1){
					dataValid = dataBuff[i+1];
					break;
				}
				i++;
			}
			UARTprintf("%s\n", dataBuff);
    	}

    	UARTprintf("%s\n", dataBuff);
		xQueueSendToBack(xSdDataQueue, dataBuff, 0);
		vTaskDelay(1000);
	}
}
void vTemp_Task(void *pvParameters) {
	uint32_t ui32ADC0Value[4]; //four reads
	volatile float uTempValueC; //temp value in degree
	volatile float uTempAvg; //temp avg from reads
	volatile float temp = 0.0;
	char * aTemp = 0;

	for(;;){
		ROM_ADCIntClear(ADC0_BASE,1); //clear the interrupt to proceed
		ROM_ADCProcessorTrigger(ADC0_BASE,1); //processor to trigger ADC
		while(!ADCIntStatus(ADC0_BASE,1,false)){
			//nothing until processor triggers ADC complete
		}
		//get the value from ADC, store in ADC0Value
		ROM_ADCSequenceDataGet(ADC0_BASE,1,ui32ADC0Value);
		//Find the average temp across the 4 reads
		uTempAvg = (ui32ADC0Value[0] +ui32ADC0Value[1] +ui32ADC0Value[2]+ui32ADC0Value[3])/4;
		//convert mV to C
		uTempValueC = 25+(745.0-uTempAvg)/2.0;
		temp=uTempValueC;
		//cast float to type char* for queue processing
		sprintf(aTemp,"%d\n",(int)(temp*10)); //need to divide by 10 in software
		UARTprintf("%d\n",(int)(temp*10)); //to print to UART for debug
		//send data to sd queue
		xQueueSendToBack(xSdDataQueue,aTemp,0);
		vTaskDelay(1000);
	}

}

