//*****************************************************************************
//
// freertos_demo.c - Simple FreeRTOS example.
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.0.12573 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include "ENGG4810.h"

//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>FreeRTOS Example (freertos_demo)</h1>
//!
//! This application demonstrates the use of FreeRTOS on Launchpad.
//!
//! The application blinks the user-selected LED at a user-selected frequency.
//! To select the LED press the left button and to select the frequency press
//! the right button.  The UART outputs the application status at 115,200 baud,
//! 8-n-1 mode.
//!
//! This application utilizes FreeRTOS to perform the tasks in a concurrent
//! fashion.  The following tasks are created:
//!
//! - An LED task, which blinks the user-selected on-board LED at a
//!   user-selected rate (changed via the buttons).
//!
//! - A Switch task, which monitors the buttons pressed and passes the
//!   information to LED task.
//!
//! In addition to the tasks, this application also uses the following FreeRTOS
//! resources:
//!
//! - A Queue to enable information transfer between tasks.
//!
//! - A Semaphore to guard the resource, UART, from access by multiple tasks at
//!   the same time.
//!
//! - A non-blocking FreeRTOS Delay to put the tasks in blocked state when they
//!   have nothing to do.
//!
//! For additional details on FreeRTOS, refer to the FreeRTOS web page at:
//! http://www.freertos.org/
//
//*****************************************************************************
//define lumiosity registers and values

#define SLAVE_ADDRESS 	0x29 //ADDR pin is selected to ground
#define NUM_I2C_DATA 	15


//define accelerometer registers and values
#define ACCEL_SLAVE_ADDR 0x53
#define ACCEL_DATA_REG 0x32
#define DATA_FORMAT_REG 0x31
#define WRITE_BYTE_NUMB 30


#define BARO_WRITE			0xC0
#define BARO_READ			0xC1

#define BARO_SLAVE_ADDR 	0x60
//*****************************************************************************
// Declarations
//*****************************************************************************
void vFATFS_Timer_Task(void* pvParameters);
void vSD_Task(void);
void vAccelerometer_Task(void *pvParameters);

void vApplicationIdleHook( void );

//void vGPS_Task(void *pvParameters);
//void vData_Retrieve_Task(void *pvParameters);
//*****************************************************************************
// The stack sizes for tasks.
//*****************************************************************************
#define mainSD_TASK_STACK					( configMINIMAL_STACK_SIZE * 3 )
#define mainFATFS_TASK_STACK				( configMINIMAL_STACK_SIZE * 3 )
#define mainACCELEROMETER_STACK				( configMINIMAL_STACK_SIZE * 3 )
#define mainGPS_STACK						( configMINIMAL_STACK_SIZE * 3 )
#define mainDATA_RETRIEVE_STACK				( configMINIMAL_STACK_SIZE * 3 )
//#define PRIORITY_ACCELEROMETER				1

//*****************************************************************************
// The item size and queue size for the LED message queue.
//*****************************************************************************

#define SD_ITEM_SIZE           sizeof(uint8_t)
#define SD_QUEUE_SIZE          128

//*****************************************************************************
// The following are data structures used by FatFs.
//*****************************************************************************

static FATFS g_sFatFs;
static DIR g_sDirObject;
static FILINFO g_sFileInfo;


uint8_t haveFix=0;
//*****************************************************************************
// The mutex that protects concurrent access of UART from multiple tasks.
//*****************************************************************************
xSemaphoreHandle accelSemaphore;

xSemaphoreHandle g_pUARTSemaphore;
//Queue handles
xQueueHandle xSdDataQueue;
extern xQueueHandle g_pLEDQueue;
//*****************************************************************************
// The error routine that is called if the driver library encounters an error.
//*****************************************************************************

#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}

#endif

typedef struct {
	uint8_t fixes; 			//number of sattelites unsigned
	uint16_t year;			//unsigned
	uint8_t month;			//unsigned
	uint8_t day;			//unsigned
	uint8_t hour;			//unsigned
	uint8_t min;			//unsigned
	uint8_t sec;			//unsigned
	int32_t lon;			//signed 2's complement
	int32_t lat;			//signed 2's complement
	int16_t XData;			//signed accelerometer data
	int16_t YData;			//signed accelerometer data
	int16_t ZData; 			//signed accelerometer data
	int16_t tempVal;		//temperature value
	uint16_t lux;			//lux value
	uint32_t pressure;		//pressure value
	uint32_t hAcc;			//unsigned
	uint32_t vAcc;			//unsigned
}__attribute__((packed)) PacketStructure;

typedef struct {
	int32_t lat;			//signed 2's complement
	int32_t lon;			//signed 2's complement
	uint8_t high;
}__attribute__((packed)) ConfigStructure;

void vApplicationIdleHook(void){
//	UARTprintf("idle\n");
	SysCtlDeepSleepClockSet(SYSCTL_DSLP_DIV_16  | SYSCTL_DSLP_OSC_MAIN |SYSCTL_DSLP_PIOSC_PD|SYSCTL_DSLP_MOSC_PD );
	SysCtlDeepSleep();
//	SysCtlPeripheralClockGating(1);
//	SysCtlSleep();
}

//void vWAKE_ISR_Handler( void ) __attribute__((noinline));


void vWAKE_ISR_Handler(void){
	int intReg;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	if(GPIOIntStatus(GPIO_PORTB_BASE,1) == 1){
		UARTprintf("into interrupt \r\n");
		GPIOIntClear(GPIO_PORTB_BASE, GPIO_PIN_0);

		if(accelSemaphore != NULL){
			xSemaphoreGiveFromISR(accelSemaphore, &xHigherPriorityTaskWoken);
		}

	}




	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);

}

void sendUBXPkt(){
	int i;
	int bufCnt = 0;
	uint8_t temp;
//	uint8_t pollUBX[8] = {0xb5, 0x62, 0x01, 0x07, 0x00, 0x00, 0x08, 0x09};
	uint8_t MsgRate17[16] = {0xB5 , 0x62 , 0x06 , 0x01 , 0x08 , 0x00 , 0x01 , 0x07 , 0x00 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00 , 0x18 , 0xE1};
	uint8_t MsgRate[16] = {0xB5 , 0x62 , 0x06 , 0x01 , 0x08 , 0x00 , 0x01 , 0x21 , 0x00 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00 , 0x32 , 0x97};
//	uint8_t MsgRate17[10] = {0xb5 , 0x62 , 0x06 , 0x01 , 0x02 , 0x00, 0x01, 0x07 , 0x11 , 0x3a};
	while(1){
		for(i=0;i<16;i++){
			UARTCharPut(UART1_BASE, MsgRate17[i]);
		}

		bufCnt++;
		if(bufCnt==400){
			break;
		}
	}

}
void sendUBXRates(){
	int i;
	//const uint8_t pollUBX[8] = {0xb5, 0x62, 0x01, 0x07, 0x00, 0x00, 0x08, 0x09};
	uint8_t GPSoff[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4D, 0x3B};
	uint8_t MsgRate16[11]  = {0xb5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x06, 0x00, 0x11, 0x4E};
	uint8_t MsgRate112[11] = {0xb5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x12, 0x00, 0x1d, 0x66};
	//uint8_t MsgRate12[11] = {0xb5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x02, 0x00, 0x0d, 0x46};
//	uint8_t MsgRate13[11] = {0xb5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x03, 0x00, 0x0e, 0x48};
	uint8_t MsgRate51[11] = {0xb5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x05, 0x01, 0x00, 0x10, 0x50};
//	uint8_t MsgRate17[11] = {0xb5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x07, 0x01, 0x13, 0x51};
	uint8_t MsgRate17[16] = {0xB5 , 0x62 , 0x06 , 0x01 , 0x08 , 0x00 , 0x01 , 0x07 , 0x00 , 0x01 , 0x01 , 0x00 , 0x00 , 0x00 , 0x19 , 0xE5};
	uint8_t MsgRate[16] = {0xB5 , 0x62 , 0x06 , 0x01 , 0x08 , 0x00 , 0x01 , 0x21 , 0x00 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00 , 0x32 , 0x97};
	uint8_t MsgRate2[16] = {0xB5 , 0x62 , 0x06 , 0x01 , 0x08 , 0x00 , 0x01 , 0x2 , 0x00 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00 , 0x13 , 0xbe};
	uint8_t MsgRate3[16] = {0xB5 , 0x62 , 0x06 , 0x01 , 0x08 , 0x00 , 0x01 , 0x03 , 0x00 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00 , 0x14 , 0xc5};
//	uint8_t MsgRate4[16] = {0xB5,0x62,0x06,0x01,0x08,0x00,0x01,0x20,0x00,0x01,0x00,0x00,0x00,0x00,0x31,0x90};

/*
	for(i=0;i<16;i++){
		UARTCharPut(UART1_BASE, GPSoff[i]);
	}
*/
	for(i=0;i<11;i++){
		UARTCharPut(UART1_BASE, MsgRate16[i]);
	}
	for(i=0;i<11;i++){
		UARTCharPut(UART1_BASE, MsgRate112[i]);
	}
/*
	for(i=0;i<11;i++){
		UARTCharPut(UART1_BASE, MsgRate12[i]);
	}

	for(i=0;i<11;i++){
		UARTCharPut(UART1_BASE, MsgRate13[i]);
	}
	*/
	for(i=0;i<11;i++){
		UARTCharPut(UART1_BASE, MsgRate51[i]);
	}
	for(i=0;i<16;i++){
		UARTCharPut(UART1_BASE, MsgRate[i]);
	}
	for(i=0;i<16;i++){
		UARTCharPut(UART1_BASE, MsgRate2[i]);
	}
	for(i=0;i<16;i++){
		UARTCharPut(UART1_BASE, MsgRate3[i]);
	}

	/*
	for(i=0;i<16;i++){
		UARTCharPut(UART1_BASE, MsgRate4[i]);
	}
	*/
}
void setLED(uint8_t numb){
	uint8_t ui8Message = numb;

	xQueueSendToBack(g_pLEDQueue, &ui8Message, 0);
	xSemaphoreGive(g_pUARTSemaphore);
}
unsigned int CalculateLux(unsigned int iGain, unsigned int tInt, unsigned int ch0,
unsigned int ch1, int iType) {
	unsigned long chScale;
	unsigned long channel1;
	unsigned long channel0;
	switch (tInt)
	{
		case 0: // 13.7 msec
			chScale = CHSCALE_TINT0;
			break;
		case 1: // 101 msec
			chScale = CHSCALE_TINT1;
			break;
		default: // assume no scaling
			chScale = (1 << CH_SCALE);
			break;
	}
	// scale if gain is NOT 16X
	if (!iGain) chScale = chScale << 4; // scale 1X to 16X
	// scale the channel values
	channel0 = (ch0 * chScale) >> CH_SCALE;
	channel1 = (ch1 * chScale) >> CH_SCALE;
	// find the ratio of the channel values (Channel1/Channel0)
	// protect against divide by zero
	unsigned long ratio1 = 0;
	if (channel0 != 0) ratio1 = (channel1 << (RATIO_SCALE+1)) / channel0;
	// round the ratio value
	unsigned long ratio = (ratio1 + 1) >> 1;
	// is ratio <= eachBreak ?
	unsigned int b, m;
	switch (iType)
	{
		case 0: // T package
			if ((ratio >= 0) && (ratio <= K1T))
			{b=B1T; m=M1T;}
			else if (ratio <= K2T)
			{b=B2T; m=M2T;}
			else if (ratio <= K3T)
			{b=B3T; m=M3T;}
			else if (ratio <= K4T)
			{b=B4T; m=M4T;}
			else if (ratio <= K5T)
			{b=B5T; m=M5T;}
			else if (ratio <= K6T)
			{b=B6T; m=M6T;}
			else if (ratio <= K7T)
			{b=B7T; m=M7T;}
			else if (ratio > K8T)
			{b=B8T; m=M8T;}
			break;
		case 1:// CS package
			if ((ratio >= 0) && (ratio <= K1C))
			{b=B1C; m=M1C;}
			else if (ratio <= K2C)
			{b=B2C; m=M2C;}
			else if (ratio <= K3C)
			{b=B3C; m=M3C;}
			else if (ratio <= K4C)
			{b=B4C; m=M4C;}
			else if (ratio <= K5C)
			{b=B5C; m=M5C;}
			else if (ratio <= K6C)
			{b=B6C; m=M6C;}
			else if (ratio <= K7C)
			{b=B7C; m=M7C;}
			else if (ratio > K8C)
			{b=B8C; m=M8C;}
			break;
	}
	unsigned long temp;
	temp = (channel0*b) - (channel1*m);
	// do not allow negative lux value
	if (temp<0) {
		temp = 0;
	}
	temp +=(1<<(LUX_SCALE-1));
	unsigned long lux = temp>>LUX_SCALE;
	return lux;
}


//*****************************************************************************
// This hook is called by FreeRTOS when an stack overflow error is detected.
//*****************************************************************************

void vApplicationStackOverflowHook(xTaskHandle *pxTask, char *pcTaskName) {

    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.

    while(1) {}
}
void ConfigI2C1(void){
//	ROM_FPULazyStackingEnable();
//	ROM_FPUEnable();
	//
	// The I2C2 peripheral must be enabled before use.
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);

	//
	// I2C2 is used with PortE[5:4].
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

	//
	// Configure the pin muxing for I2C2 functions on port E4 and E5.
	// This step is not necessary if your part does not support pin muxing
	//
	GPIOPinConfigure(GPIO_PA6_I2C1SCL);
	GPIOPinConfigure(GPIO_PA7_I2C1SDA);

	//
	// Select the I2C function for these pins.  This function will also
	// configure the GPIO pins pins for I2C operation, setting them to
	// open-drain operation with weak pull-ups.
	GPIOPinTypeI2CSCL(GPIO_PORTA_BASE, GPIO_PIN_6);
	GPIOPinTypeI2C(GPIO_PORTA_BASE, GPIO_PIN_7);

	//
	// Enable and initialize the I2C2 master module.  Use the system clock for
	// the I2C2 module.  The last parameter sets the I2C data transfer rate.
	// If false the data rate is set to 100kbps and if true the data rate will
	// be set to 400kbps.  For this example we will use a data rate of 100kbps.
	//
	I2CMasterInitExpClk(I2C1_BASE, SysCtlClockGet(), false);

}
void ConfigLuxI2C(void){
	//enable i2c and pin B3 and B2
		 SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
		 SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
		 GPIOPinConfigure(GPIO_PB2_I2C0SCL);
		 GPIOPinConfigure(GPIO_PB3_I2C0SDA);

		 //select i2c
		 GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
		 GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

		 //Initialise the master
		 I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false);

}


void ConfigI2C2(void){
	//
	    // The I2C2 peripheral must be enabled before use.
	    //
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C2);

	    //
	    // I2C2 is used with PortE[5:4].
	    //
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	    //
	    // Configure the pin muxing for I2C2 functions on port E4 and E5.
	    // This step is not necessary if your part does not support pin muxing
	    //
	    GPIOPinConfigure(GPIO_PE4_I2C2SCL);
	    GPIOPinConfigure(GPIO_PE5_I2C2SDA);

	    //
	    // Select the I2C function for these pins.  This function will also
	    // configure the GPIO pins pins for I2C operation, setting them to
	    // open-drain operation with weak pull-ups.
	    GPIOPinTypeI2CSCL(GPIO_PORTE_BASE, GPIO_PIN_4);
	    GPIOPinTypeI2C(GPIO_PORTE_BASE, GPIO_PIN_5);

	    //
	    // Enable and initialize the I2C2 master module.  Use the system clock for
	    // the I2C2 module.  The last parameter sets the I2C data transfer rate.
	    // If false the data rate is set to 100kbps and if true the data rate will
	    // be set to 400kbps.  For this example we will use a data rate of 100kbps.
	    //
	    I2CMasterInitExpClk(I2C2_BASE, SysCtlClockGet(), false);

}



void ConfigureADC(void){
	  //enable floating point integers
	    ROM_FPULazyStackingEnable();
	    ROM_FPUEnable();
	    //set up the system clock to run at 40MHz
//	    ROM_SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

	    //enable the ADC peripheral ADC0
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	    //enable gpio port E
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	    //Set the internal reference (3V)
	    ROM_ADCReferenceSet(ADC0_BASE,ADC_REF_INT);

	    //configure the GPIO port E, to port 3
	    ROM_GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_3);

	    //disable ADC prior to use to make use accurate (due to previous initializations)
	    ROM_ADCSequenceDisable(ADC0_BASE,1);

	    //trigger the ADC sequencer
	    ROM_ADCSequenceConfigure(ADC0_BASE,1,ADC_TRIGGER_PROCESSOR,0);


	    //set the interrupt flag when sampling is done and read from AIN0
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,0,ADC_CTL_CH0);
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,1,ADC_CTL_CH0);
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,2,ADC_CTL_CH0);
	    ROM_ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_CH0|ADC_CTL_IE|ADC_CTL_END);

	    //now enable 3rd sequencer
	    ROM_ADCSequenceEnable(ADC0_BASE,1);
}






//*****************************************************************************
// Configure the UART and its pins.  This must be called before UARTprintf().
//*****************************************************************************

void ConfigureUART(void) {
	 //
	    // Enable the GPIO Peripheral used by the UART.
	    //
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); //usb communication
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); //GPS communication
	    //
	    // Enable UART0 and UART1
	    //
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
	    //
	    // Configure GPIO Pins for UART mode.
	    //
	    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
	    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
	    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	    ROM_GPIOPinConfigure(GPIO_PC4_U1RX);
	    ROM_GPIOPinConfigure(GPIO_PC5_U1TX);
	    ROM_GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);
	    //
	    // Use the internal 16MHz oscillator as the UART clock source.
	    //
	    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
	    UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);

	    //
	    // Initialize the UART for console I/O.
	    //
	    UARTStdioConfig(0, 115200, 16000000);
	    UARTConfigSetExpClk(UART1_BASE, 16000000, 38400, UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE);//115200

}

void LuxI2CStartup(void){
	//need to initialise light sensor, set to write
	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false);
	 I2CMasterDataPut(I2C0_BASE,0x00); //power up
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SEND_START);

	 while(I2CMasterBusy(I2C0_BASE));
	 I2CMasterDataPut(I2C0_BASE,0x03); //power up
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SEND_FINISH);
	 while(I2CMasterBusy(I2C0_BASE));
	 //
	 //
	 //should there be something else to set the register value here?
	 //
	 //

}
void BaroStartup(void){
	//set master to send and delay to ensure it is set
	I2CMasterSlaveAddrSet(I2C1_BASE, BARO_SLAVE_ADDR, false);
	I2CMasterDataPut(I2C1_BASE, 0x12); // Send command to start conversions
	I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_START);
	while(I2CMasterBusy(I2C1_BASE)); //wait until the master has finished operation

	// Send command to get data
	I2CMasterDataPut(I2C1_BASE, 0x00);
	I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
	while(I2CMasterBusy(I2C1_BASE)); //wait until the master has finished operation

	vTaskDelay(50);
}
void I2CStartup(void){
		//turn off interrupts
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);
		I2CMasterDataPut(I2C2_BASE, 0x2E);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0x00);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};


	//set master to send and delay to ensure it is set
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);

		//place in +-16g mode
		I2CMasterDataPut(I2C2_BASE, DATA_FORMAT_REG);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0x0B);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};

		//set up interrupts



		//Make activity threshold 1
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);
		I2CMasterDataPut(I2C2_BASE, 0x24);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0x05);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};

		//set Act_inact_ctrl
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);
		I2CMasterDataPut(I2C2_BASE, 0x27);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0xc0);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};

		//set int map
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);
		I2CMasterDataPut(I2C2_BASE, 0x2F);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0xEF);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};

		//enable interrupt
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);
		I2CMasterDataPut(I2C2_BASE, 0x2E);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0x10);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);

		I2CMasterDataPut(I2C2_BASE, 0x2D);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0x08);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};



}

void shutDownICs(int accel){

	if(accel){
		//shut down the accelerometer
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);

		//accelerometer
		I2CMasterDataPut(I2C2_BASE, 0x2D);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
		while(I2CMasterBusy(I2C2_BASE)){};

		I2CMasterDataPut(I2C2_BASE, 0x00);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
		while(I2CMasterBusy(I2C2_BASE)){};


		I2CMasterDisable(I2C2_BASE);
	}
	//luminosity

	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false);
	 I2CMasterDataPut(I2C0_BASE,0x00); //cmd reg
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SEND_START);

	 while(I2CMasterBusy(I2C0_BASE));
	 I2CMasterDataPut(I2C0_BASE,0x00); //power down
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SEND_FINISH);
	 while(I2CMasterBusy(I2C0_BASE));



	//barometer
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_4);
	ROM_GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, 0x00);


	I2CMasterDisable(I2C1_BASE);
	I2CMasterDisable(I2C0_BASE);

	SysCtlPeripheralPowerOff(SYSCTL_PERIPH_ADC0);
	SysCtlPeripheralPowerOff(I2C1_BASE);
	SysCtlPeripheralPowerOff(I2C0_BASE);
	SysCtlPeripheralDisable(SYSCTL_PERIPH_ADC0);
	SysCtlPeripheralDisable(I2C1_BASE);
	SysCtlPeripheralDisable(I2C0_BASE);
	SysCtlDelay(2);

	//ROM_ADCSequenceDisable(ADC0_BASE,1);
}
void startUpICs(void){

	//ROM_ADCSequenceEnable(ADC0_BASE,1);
	SysCtlPeripheralPowerOn(SYSCTL_PERIPH_ADC0);
	SysCtlPeripheralPowerOn(I2C1_BASE);
	SysCtlPeripheralPowerOn(I2C0_BASE);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlPeripheralEnable(I2C1_BASE);
	SysCtlPeripheralEnable(I2C0_BASE);
	SysCtlDelay(2);

	I2CMasterEnable(I2C2_BASE);
	I2CMasterEnable(I2C1_BASE);
	I2CMasterEnable(I2C0_BASE);
	//wake the accelerometer
	I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);

	I2CMasterDataPut(I2C2_BASE, 0x2D);
	I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
	while(I2CMasterBusy(I2C2_BASE)){};

	I2CMasterDataPut(I2C2_BASE, 0x08);
	I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
	while(I2CMasterBusy(I2C2_BASE)){};

	//need to initialise light sensor, set to write
	I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false);
	I2CMasterDataPut(I2C0_BASE,0x00); //cmd reg
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SEND_START);

	while(I2CMasterBusy(I2C0_BASE));
	I2CMasterDataPut(I2C0_BASE,0x03); //power up
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SEND_FINISH);
	while(I2CMasterBusy(I2C0_BASE));

	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	ROM_GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_4);
	ROM_GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, 0xFF);

}

void getBaroData(PacketStructure * dataBuff){
	char accData[64];
	uint16_t baroData[12];
	uint16_t pressure, temp;
	int16_t Padc, Tadc, a0, b1, b2, c12;
	float a0_float, b1_float, b2_float, c12_float;
	float pressureComp, pressureActual;
	float Pf, Tf;
	float calc_1, calc_2, calc_3, calc_4;
	int regNum = 0;
	for(regNum = 0; regNum < 12; regNum++){
				baroData[regNum] = 0;
	}

	BaroStartup();

	I2CMasterSlaveAddrSet(I2C1_BASE, BARO_SLAVE_ADDR, false);
	I2CMasterDataPut(I2C1_BASE, 0x00);
	I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_SINGLE_SEND);
	while(I2CMasterBusy(I2C1_BASE)); //wait until the master has finished operation

	//Change to read address into buffer and delay to ensure it has set
	I2CMasterSlaveAddrSet(I2C1_BASE, BARO_SLAVE_ADDR, true);

	for(regNum = 0; regNum < 12; regNum++) {
		if(regNum == 0){ //starting packet, register 1
			I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
		}else if(regNum==11){ //ending packet, register 6
			I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
		}else{// registers 2 to 11 inclusive
			I2CMasterControl(I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
		}
		while(I2CMasterBusy(I2C1_BASE));

		baroData[regNum] = I2CMasterDataGet(I2C1_BASE);
	}

	Padc = (baroData[0] << 8)|(baroData[1]);
	Tadc = (baroData[2] << 8)|(baroData[3]);
	a0 = (baroData[4] << 8)|(baroData[5]);
	b1 = (baroData[6] << 8)|(baroData[7]);
	b2 = (baroData[8] << 8)|(baroData[9]);
	c12 = ((baroData[10] << 8)|(baroData[11])) >> 2;

	pressure = Padc >> 6;
	temp = Tadc >> 6;

	Pf = pressure;
	Tf = temp;

	a0_float = a0/8;
	b1_float = b1/8192;
	b2_float = b2/16384;
	c12_float = c12/4194304;

	calc_1 = c12_float*Tf;
	calc_2 = calc_1 + b1_float;
	calc_3 = calc_2 * Pf;
	calc_4 = b2_float * Tf;

	pressureComp = a0_float + calc_3 + calc_4;

	pressureActual = (pressureComp * 63.538611925708699902248289345064) + 50000;

	dataBuff->pressure = (uint32_t)pressureActual;

}
void getI2CData(PacketStructure * dataBuff){
	int regNum = 0;//always less than 7
	uint8_t allData[6] = {0,0,0,0,0,0};
	int16_t XData;
	int16_t YData;
	int16_t ZData;
	//send write address and data register to read from into buffers and send to Accelerometer
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);

		I2CMasterDataPut(I2C2_BASE,  ACCEL_DATA_REG);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_SINGLE_SEND);
		while(I2CMasterBusy(I2C2_BASE)); //wait until the master has finished operation

		//Change to read address into buffer and delay to ensure it has set
		I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,true);

		//Receive multiple bytes from the accelerometer
		for(regNum = 0; regNum<6;regNum++){
			//use the appropriate master control function
			if(regNum ==0){ //starting packet, register 1
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
			}else if(regNum==5){ //ending packet, register 6
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
			}else{// registers 2 to 5 inclusive
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			}
			while(I2CMasterBusy(I2C2_BASE));

			allData[regNum] = I2CMasterDataGet(I2C2_BASE);
			//UARTprintf("%d \n", allData[regNum]);

		}
    	XData = ((allData[1]<<8)|allData[0]);
    	YData = ((allData[3]<<8)|allData[2]);
    	ZData = ((allData[5]<<8)|allData[4]);
    	dataBuff->XData = XData;
    	dataBuff->YData = YData;
    	dataBuff->ZData = ZData;

    	//UARTprintf("X:%d, Y:%d, Z:%d \n",dataBuff->XData, dataBuff->YData, dataBuff->ZData);


}
void GPSPow(int mode){
//	const uint8_t GPSon[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4C, 0x37};
//	const uint8_t GPSoff[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4D, 0x3B};
	const uint8_t GPSoff[] = {0xB5,0x62,0x06,0x04,0x04,0x00,0x00,0x00,0x08,0x00,0x16,0x74};
	const uint8_t GPSon[] = {0xB5,0x62,0x06,0x04,0x04,0x00,0x00,0x00,0x09,0x00,0x17,0x76};
	uint8_t * sendGPS;
	int i;
	if(mode){
		sendGPS = GPSon;
	}else{
		sendGPS = GPSoff;
	}

	for(i=0;i<12;i++){
		UARTCharPut(UART1_BASE, sendGPS[i]);
	}
}

void sendGPSPowMode(int mode){
/*	const uint8_t GPSon[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4C, 0x37};
	const uint8_t GPSoff[] = {0xB5, 0x62, 0x02, 0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4D, 0x3B};
	const uint8_t GPSMaxPower[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x00, 0x21, 0x91};
	const uint8_t GPSEcoMode[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x04, 0x25, 0x95};
*/

	const uint8_t PowSaveCFG[] = {0xB5,0x62,0x06,0x3B,0x2C,0x00,0x01,0x06,0x00,0x00,0x00,0x9C,0x03,0x00,0xE8,0x03,0x00,0x00,0x10,
									0x27,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x2C,0x01,0x00,0x00,0x4F,0xC1,0x03,0x00,0x87,
										0x02,0x00,0x00,0xFF,0x00,0x00,0x00,0x64,0x40,0x01,0x00,0xA4,0xD3};
	const uint8_t PowSaveCFG2[] = {0xB5,0x62,0x06,0x3B,0x2C,0x00,0x01,0x06,0x00,0x00,0x00,0x90,0x00,0x00,0x60,0xEA,0x00,0x00,0xC0,
									0x27,0x09,0x00,0x00,0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x2C,0x01,0x00,0x00,0x4F,0xC1,0x03,0x00,0x87,
										0x02,0x00,0x00,0xFF,0x00,0x00,0x00,0x64,0x40,0x01,0x00,0xAD,0x10};
	uint8_t DataRate[14]= {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xE8, 0x03, 0x01, 0x00, 0x01, 0x00, 0x01, 0x39};
	uint8_t DataRate2[14] = {0xB5 ,0x62, 0x06, 0x08, 0x06, 0x00, 0x60, 0xEA, 0x01, 0x00, 0x01, 0x00, 0x60, 0x8C};
	const uint8_t GPSPowSave[] = {0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x01, 0x22, 0x92};

	int i;
	if(mode){
		for(i=0;i<10;i++){
			UARTCharPut(UART1_BASE, GPSPowSave[i]);
		}

		for(i=0;i<52;i++){
			UARTCharPut(UART1_BASE, PowSaveCFG[i]);
		}

	}
	for(i=0;i<14;i++){
		UARTCharPut(UART1_BASE, DataRate[i]);
	}

}

int ifEqual(uint8_t * start, uint8_t * buffer, int length){
	int i;
	int count=0;
	for(i=0;i<length;i++){
		if(start[i]==buffer[i]){
			count++;
		}
	}

	if(count==length){
		count=1;
	}else{
		count = 0;
	}


	return count;
}


void getUBX(uint8_t ** dataBuffer){
	int i;
	int dataFinished=0;
	int storeStuff = 0;
	int bufCnt = 0;
	int timeDone = 0;
	int posDone = 0;
	uint8_t temp;
//	uint8_t start1[6] = {0xb5, 0x62, 0x01, 0x07, 0x54, 0x00};
	uint8_t start1[6] = {0xb5, 0x62, 0x01, 0x21, 0x14, 0x00};
	uint8_t start2[6] = {0xb5, 0x62, 0x01, 0x02, 0x1c, 0x00};
	uint8_t start3[6] = {0xb5, 0x62, 0x01, 0x03, 0x10, 0x00};
	uint8_t start4[6] = {0xb5, 0x62, 0x01, 0x20, 0x10, 0x00};
	uint8_t buffer[6] = {0,0,0,0,0,0};
//	uint8_t dataBuffT[20];
//	uint8_t dataBuffP[28];


	while(dataFinished==0){
		temp = UARTCharGet(UART1_BASE);
		//UARTprintf("%x ", temp);
		if(storeStuff==0){

			buffer[0] = buffer[1];
			buffer[1] = buffer[2];
			buffer[2] = buffer[3];
			buffer[3] = buffer[4];
			buffer[4] = buffer[5];
			buffer[5] = temp;

			if(ifEqual(start1, buffer, 6)){

				storeStuff=1;
			}else if(ifEqual(start2, buffer,6)){
				storeStuff=2;
			}else if (ifEqual(start3, buffer, 6)){
				storeStuff=3;
			}else if(ifEqual(start4,buffer,6)){
				storeStuff=4;
			}

		}else{
			if((storeStuff==1)){
				//UARTprintf("%x ", temp);
				dataBuffer[0][bufCnt] = temp;
				if(bufCnt==19){
					dataFinished+=1;
				//	UARTprintf("\n");
				}
				bufCnt++;
			}else if((storeStuff==2)){

				dataBuffer[1][bufCnt] = temp;
				if(bufCnt==27){

					dataFinished+=1;

				}
				bufCnt++;
			}else if(storeStuff==3){

				dataBuffer[2][bufCnt] = temp;
				if(bufCnt==15){

					dataFinished+=1;

				}
				bufCnt++;

			}else if(storeStuff==4){

				if(bufCnt==15){
					dataFinished+=1;

				}
				bufCnt++;
			}


		}

	}
	//UARTprintf("\n");


}


void getGPSData(PacketStructure * dataBuff){
	int i;
	uint8_t temp;
	int commas = 0;
	int cntr=0;
	uint8_t * gpsDataBuffs[3];
	uint8_t gpsT[20];
	uint8_t gpsP[28];
	uint8_t gpsV[16];


	gpsDataBuffs[0] = gpsT;
	gpsDataBuffs[1] = gpsP;
	gpsDataBuffs[2] = gpsV;

	getUBX(gpsDataBuffs);
	getUBX(gpsDataBuffs);
	getUBX(gpsDataBuffs);
	if((gpsV[5]&1)==1){
		haveFix = 1;
		//UARTprintf("DataValid %d %d %d ,,,", gpsT[12], gpsT[13], gpsT[19]);
	}else{
		//UARTprintf("DataNotValid ");
	}

/*
	while(gpsT[19]!=7){//recieve again if transmission is disrupted
		getUBX(gpsDataBuffs);
		getUBX(gpsDataBuffs);
		getUBX(gpsDataBuffs);
	}
*/
	dataBuff->fixes = gpsV[4];
	dataBuff->year = (gpsT[13]<<8)+(gpsT[12]);
	dataBuff->month = gpsT[14];
	dataBuff->day = gpsT[15];
	dataBuff->hour =gpsT[16];
	dataBuff->min = gpsT[17];
	dataBuff->sec = gpsT[18];
	dataBuff->lon = (gpsP[7]<<24)+(gpsP[6]<<16)+(gpsP[5]<<8)+gpsP[4];
	dataBuff->lat = (gpsP[11]<<24)+(gpsP[10]<<16)+(gpsP[9]<<8)+gpsP[8];
	dataBuff->hAcc = (gpsP[23]<<24)+(gpsP[22]<<16)+(gpsP[21]<<8)+gpsP[20];
	dataBuff->vAcc = (gpsP[27]<<24)+(gpsP[26]<<16)+(gpsP[25]<<8)+gpsP[24];

}

void getTempData(PacketStructure * dataBuff){
	uint32_t ui32ADC0Value[4]; //four reads
	volatile float uTempValueC; //temp value in degree
	volatile float uTempAvg; //temp avg from reads
	volatile float temp = 0.0;
	char * aTemp = 0;


	ROM_ADCIntClear(ADC0_BASE,1); //clear the interrupt to proceed
	ROM_ADCProcessorTrigger(ADC0_BASE,1); //processor to trigger ADC
	while(!ADCIntStatus(ADC0_BASE,1,false)){
		//nothing until processor triggers ADC complete
	}
	//get the value from ADC, store in ADC0Value
	ROM_ADCSequenceDataGet(ADC0_BASE,1,ui32ADC0Value);
	//Find the average temp across the 4 reads
	uTempAvg = (ui32ADC0Value[0] +ui32ADC0Value[1] +ui32ADC0Value[2]+ui32ADC0Value[3])/4;
	//convert mV to C
	uTempValueC = 25+(745.0-uTempAvg)/2.0;
	temp=uTempValueC;
	//cast float to type char* for queue processing
	dataBuff->tempVal = (int16_t)(temp*10); //need to divide by 10 in software

}
void getLux(PacketStructure * dataBuff){
	unsigned int byte1; //byte 1
	unsigned int byte2; //byte 2
	unsigned int channel0;
	unsigned int channel1;

	// write command for channel 0 select
	I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false); //slave address may be 0x29 or 0x49 depend
	I2CMasterDataPut(I2C0_BASE,0xAC);
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_SINGLE_SEND); //command channel
	while(I2CMasterBusy(I2C0_BASE));

	//read in channel 0
	I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,true);
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_START);
	while(I2CMasterBusy(I2C0_BASE));
	byte1 = I2CMasterDataGet(I2C0_BASE); //read into byte 1
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //receive next byte
	while(I2CMasterBusy(I2C0_BASE)); //wait until end
	byte2 = I2CMasterDataGet(I2C0_BASE); //read next byte
	//convert to channel0 read
	channel0=256 * byte2 + byte1;

	// write command for channel 1 select
	I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false); //slave address may be 0x29 or 0x49 depend
	I2CMasterDataPut(I2C0_BASE,0xAE);
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_SINGLE_SEND); //command channel
	while(I2CMasterBusy(I2C0_BASE));

	//read in channel 1
	I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,true);
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_START);
	while(I2CMasterBusy(I2C0_BASE));
	byte1 = I2CMasterDataGet(I2C0_BASE); //read into byte 1
	I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //receive next byte
	while(I2CMasterBusy(I2C0_BASE)); //wait until end
	byte2 = I2CMasterDataGet(I2C0_BASE); //read next byte
	//convert to channel0 read
	channel1=256 * byte2 + byte1;

	//calculate lux
	dataBuff->lux = (uint16_t)CalculateLux(1,2,channel0,channel1, 0);

}

int getData(uint8_t * dataBuff, int counter, int gpsOnly, int getPos, int mode){
	PacketStructure * pktPointer = (PacketStructure *) dataBuff;
	int32_t lon;			//signed 2's complement
	int32_t lat;			//signed 2's complement
	uint32_t hAcc;
	uint8_t fixes;
	lon = pktPointer->lon;
	lat = pktPointer->lat;
	hAcc = pktPointer->hAcc;
	fixes = pktPointer->fixes;

	if(gpsOnly){
//		UARTEnable(UART1_BASE);
		getGPSData(pktPointer);
//		UARTDisable(UART1_BASE);
	}else{
//		UARTEnable(UART1_BASE);
		getGPSData(pktPointer);
//		UARTDisable(UART1_BASE);
		getI2CData(pktPointer);
		getTempData(pktPointer);
		getLux(pktPointer);
		getBaroData(pktPointer);

if(mode==2){
	if(!((getPos)&&(pktPointer->fixes==3))){
		pktPointer->fixes = fixes;
		pktPointer->lon = lon;
		pktPointer->lat = lat;
		pktPointer->hAcc = hAcc;

	}
}
	UARTprintf("%d %d %d %d %d %d %d %d\r\n",pktPointer->fixes, pktPointer->tempVal, counter, pktPointer->lat, pktPointer->lon, pktPointer->XData, pktPointer->YData, pktPointer->ZData);
	}
	return(pktPointer->fixes);
}



int checkFileName(void){
	FRESULT iFResult;
	unsigned short usBytesRead = 9;

	int mode = 0;

	iFResult = f_opendir(&g_sDirObject, "");
	if (iFResult) die(iFResult);

	// Check file names
	for(;;){
		iFResult = f_readdir(&g_sDirObject, &g_sFileInfo); 					//Read directory item
		if (iFResult || !g_sFileInfo.fname[0]) break;						// Error or end of dir

		if (!(g_sFileInfo.fattrib & AM_DIR)){ 					// Check if not directory
			if (strstr(g_sFileInfo.fname, "TRACK5") != NULL){
				mode = 1;
				break;
			} else if(strstr(g_sFileInfo.fname, "TRACK2") != NULL){
				mode = 2;
				break;
			} else {
				mode = 0;

			}
			//UARTprintf("%s\n", g_sFileInfo.fname);
		}
	}

	//UARTprintf("Mode: %d\n", mode);
	return mode;
}

void readConfigData(void){
	FIL g_sFileObject2;
	unsigned short usBytesRead = 9;
	FRESULT iFResult;
	uint8_t dataBuffer[9];
	ConfigStructure * pktPointer = (ConfigStructure *) dataBuffer;
   // Max filename length is 12 including extension. For longer, modify ffconf.h.


	iFResult = f_open(&g_sFileObject2, "track1.cfg", FA_READ);
	if (iFResult) die(iFResult);

		while(usBytesRead == sizeof(dataBuffer)) {

			if(f_eof(&g_sFileObject2)) {
				break;
			}

			f_read(&g_sFileObject2, dataBuffer, sizeof(dataBuffer), &usBytesRead);
			dataBuffer[usBytesRead] = 0;
			UARTprintf("%d, %d, %d\n", pktPointer->lon, pktPointer->lat, pktPointer->high);
			UARTprintf("Bytes Read: %d\n", usBytesRead);

		}
}

void buzzerState(int state){
	if(state){
	    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_6);
	    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_6, 0x00);
	}else{
	    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_6);
	    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_6, 0xff);
	}
}
//*****************************************************************************
// Initialise FreeRTOS and start the initial set of tasks.
//*****************************************************************************
int main(void) {

    f_mount(0, &g_sFatFs);

    // Set the clocking to run at 50 MHz from the PLL.
//    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
//                       SYSCTL_OSC_MAIN);
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_16|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

    // Enable the peripherals used by this example.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);

    // Initialise the UART and configure it for 115,200, 8-N-1 operation.
    ConfigureUART();
    ConfigI2C2();
    ConfigI2C1();
    ConfigureADC();
    ConfigLuxI2C();
    //config PB0 for input interrupt
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_0);

    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_6);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_6, 0xff);
    // Create a mutex to guard the UART.
    g_pUARTSemaphore = xSemaphoreCreateBinary();
    accelSemaphore = xSemaphoreCreateBinary();
    xSdDataQueue = xQueueCreate(1, 32 * sizeof(char));
//    g_pLEDQueue = xQueueCreate(5, sizeof(uint8_t));
    // Create tasks
    xTaskCreate(vSD_Task, (signed portCHAR *)"SD", mainSD_TASK_STACK, NULL,
    		PRIORITY_SD_TASK, NULL);

    xTaskCreate(vFATFS_Timer_Task, (signed portCHAR *)"FATFS", mainFATFS_TASK_STACK, NULL,
    		PRIORITY_FATFS_TASK, NULL);
/*
    xTaskCreate(vData_Retrieve_Task, (signed portCHAR *)"DATARET", mainDATA_RETRIEVE_STACK, NULL,
    		PRIORITY_DATA_RETRIEVE, NULL);
    		*/

    xTaskCreate(vAccelerometer_Task, (signed portCHAR *)"Accelerometer", mainACCELEROMETER_STACK, NULL,
    		PRIORITY_ACCELEROMETER, NULL);
/*
    xTaskCreate(vGPS_Task, (signed portCHAR *)"GPS", mainGPS_STACK, NULL,
    		PRIORITY_GPS, NULL);
*/

    // Create the LED task.
    if(LEDTaskInit() != 0) {
        while(1) {}
    }
/*
    // Create the switch task.
    if(SwitchTaskInit() != 0) {
        while(1) {}
    }
*/
    // Start the scheduler.  This should not return.
    vTaskStartScheduler();

    // In case the scheduler returns for some reason, print an error and return 0.
    return 0;
}

//*****************************************************************************
// Tasks
//*****************************************************************************

// FATFS Timer Task - SD card requires clock every 10ms
void vFATFS_Timer_Task(void* pvParameters) {
    portTickType xLastWakeTime = xTaskGetTickCount();

    for(;;) {
        disk_timerproc();
        vTaskDelayUntil(&xLastWakeTime, (10/portTICK_RATE_MS));
    }
}

// SD Task - Write to SD card
void vSD_Task(void) {
	FIL g_sFileObject;
    FRESULT iFResult;
    char cData[128];
    char cNewFileName[64];
    int logVersion = 1;
    UINT bw;
    iFResult = f_opendir(&g_sDirObject, "");
    if (iFResult) die(iFResult);

    // Check file names
	for(;;){
		iFResult = f_readdir(&g_sDirObject, &g_sFileInfo); 					//Read directory item
		if (iFResult || !g_sFileInfo.fname[0]) break;					// Error or end of dir
		if (!(g_sFileInfo.fattrib & AM_DIR)){ 					// Check if not directory
//			if (strstr(g_sFileInfo.fname, "T8_Log") != NULL){
//				logVersion++;
//			}
			logVersion++;
		}
	}

	sprintf(cNewFileName, "T8_Log%d.txt", logVersion);

    // Max filename length is 12 including extension. For longer, modify ffconf.h.
    iFResult = f_open(&g_sFileObject, cNewFileName, FA_WRITE | FA_CREATE_ALWAYS);
    if (iFResult) die(iFResult);
    iFResult = f_sync(&g_sFileObject);
    if (iFResult) die(iFResult);

    for (;;) {
    	if (xQueueReceive(xSdDataQueue, &cData, 500) == pdTRUE) {
//    		UARTprintf("Writing:%s\n", cData);
			//Write data to file.
    		iFResult = f_write(&g_sFileObject, cData, 30, &bw);
//    		iFResult = f_printf(&g_sFileObject, "%s\r\n", cData);
    		iFResult = f_sync(&g_sFileObject);
			if (iFResult) die(iFResult);
		}
        vTaskDelay(50);
    }
}

void vAccelerometer_Task(void *pvParameters) {
//	char* accData = "test4";
	FIL g_sFileObject2;
	unsigned short usBytesRead = 9;
	FRESULT iFResult;
	uint8_t dataBuffer2[9];

	uint32_t HErrVal;
	uint32_t VErrVal;
	uint32_t lastHErrVal;
	uint32_t lastVErrVal;
	uint8_t dataBuffer[128];


	ConfigStructure * cfgPointer = (ConfigStructure *) dataBuffer2;
	PacketStructure * pktPointer = (PacketStructure *) dataBuffer;
int32_t lastLon;
int32_t lastLat;

	uint8_t intReg;
	int awake = 0;
	int routePt = 0;
	int routeCnt = 0;
	int setPowMode = 0;
	int counter = 1;
	int fixes;
	int mode = 0;
	int state = 0;
	int startTrack = 1;
	int tracking = 0;
	int doTrack = 0;
	int LEDCount=0;
	int flag = 0;
	int sleeping = 0;
	int routeNum = 0;
    uint8_t ui8Message=0;
    int errorMargin;
	portTickType xLastWakeTime = xTaskGetTickCount();
	memset(dataBuffer,0, 128*sizeof(uint8_t));

	mode=checkFileName();

	if(mode==2){
		tracking=1;
		mode=2;

	}


	//set up interrupt for PB0 and activate it
	I2CStartup();
	LuxI2CStartup();
	sendUBXRates();
	setLED(mode);

	if(mode==1){
		GPIOIntTypeSet(GPIO_PORTB_BASE, GPIO_PIN_0, GPIO_RISING_EDGE );
		GPIOIntEnable(GPIO_PORTB_BASE, GPIO_PIN_0);
		IntEnable(INT_GPIOB);
		IntMasterEnable();
		GPSPow(0);//put to sleep until interrupt
		sendGPSPowMode(0);
	}else if (mode==0){
		GPSPow(1);//need to change to 0 for wake on move
		sendGPSPowMode(0);
	}else if (mode==2){
		sendGPSPowMode(1);
		GPSPow(1);//need to change to 0 for wake on move

	}

	shutDownICs(0);




	for(;;) {
		/*	if(haveFix==1 && setPowMode==0){
				setPowMode = 1;
				sendGPSPowMode();
			}
			*/
		if(mode==1){
			if(awake){ // if awake


				UARTprintf("in startup\n");

				startUpICs();
				fixes = getData(dataBuffer, counter,0,1,mode);
				shutDownICs(0);
				//UARTprintf("in startup\n");

				if(fixes==3){
					UARTprintf("exit startup\n");
					awake=0;

					IntEnable(INT_GPIOB);
					GPSPow(0);

					counter = 1;


					xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
				}

				if(counter == 121){
					UARTprintf("exit no fix");
					awake=0;
					IntEnable(INT_GPIOB);
					GPSPow(0);
					counter = 1;


					xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
				}
				counter++;


			}else{//if not awake
				//waiting

				if(accelSemaphore != NULL){
					if(xSemaphoreTake(accelSemaphore, 10) == pdTRUE){
						awake = 1;
						//check interrupt status
						I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,false);

						I2CMasterDataPut(I2C2_BASE,  0x30);
						I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_SINGLE_SEND);
						while(I2CMasterBusy(I2C2_BASE)); //wait until the master has finished operation

						//Change to read address into buffer and delay to ensure it has set
						I2CMasterSlaveAddrSet(I2C2_BASE,ACCEL_SLAVE_ADDR,true);
						I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
						while(I2CMasterBusy(I2C2_BASE));
						intReg = I2CMasterDataGet(I2C2_BASE);
						UARTprintf("Ints: %d\r\n", intReg);


						IntDisable(INT_GPIOB);


						GPSPow(1);
					}

				}



			}
		}else if(mode==0){



			switch (state){
				case 0://2min wait
					startUpICs();
					fixes = getData(dataBuffer, counter,0,1,mode);
					shutDownICs(0);


					if(counter == 121){
						xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
						state = 1;
						counter = 1;
						sleeping = 1;
						GPSPow(0);
						UARTprintf("no fix\n");
						doTrack=1;
					}else{


						if(fixes==3){
							xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
							state = 1;
							counter = 1;
							GPSPow(0);
							sleeping = 1;
							UARTprintf("got fix\n");
							doTrack=1;
						}else{
							if(counter%61==0){
								xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
								doTrack=1;
							}
						}

					}
					counter++;
					break;
				case 1://45 sec wait

					//UARTprintf("count: %d \n", counter);
					if(counter == 46){

						UARTprintf("finish wait\n");
						sleeping = 0;
						GPSPow(1);

					}else if ((counter > 46) && (counter <= 61)){

						startUpICs();
						fixes = getData(dataBuffer, counter,0,1,mode);
						shutDownICs(0);

						if(counter == 61){
							if(fixes==3){
								xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
								state = 1;
								counter = 1;
								sleeping = 1;
								GPSPow(0);

							}else{
								UARTprintf("on\n");
								sleeping = 0;
								state=0;
								counter = 1;
								xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
							}
							doTrack=1;
						}else{
							if(fixes==3){
								doTrack=1;
								xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
								state = 1;
								counter = 1;
								sleeping = 1;
								GPSPow(0);
							}
						}


					}
					counter++;
					break;
			}

		}else if (mode == 2){
			if(counter<=60){
				startUpICs();
				fixes = getData(dataBuffer, counter,0,0,mode);
				shutDownICs(0);
				xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
				counter++;
			}else if(counter==61){
				startUpICs();
				fixes = getData(dataBuffer, counter,0,1,mode);
				shutDownICs(0);
				xQueueSendToBack(xSdDataQueue, dataBuffer, 0);
				doTrack=1;
				counter=1;

			}



		}


		if(tracking){

			if(startTrack){
				iFResult = f_open(&g_sFileObject2, "track2.cfg", FA_READ);
				if (iFResult) die(iFResult);
				while(usBytesRead == sizeof(dataBuffer2)) {

					if(f_eof(&g_sFileObject2)) {
						f_close(&g_sFileObject2);
						break;
					}

					f_read(&g_sFileObject2, dataBuffer2, sizeof(dataBuffer2), &usBytesRead);
					dataBuffer2[usBytesRead] = 0;

					lastLat = cfgPointer->lat;
					lastLon = cfgPointer->lon;
					routeNum++;
				}
				startTrack=0;
			}

			if(doTrack){
				iFResult = f_open(&g_sFileObject2, "track2.cfg", FA_READ);
						if (iFResult) die(iFResult);
				while(usBytesRead == sizeof(dataBuffer2)) {

					if(f_eof(&g_sFileObject2)) {



						if((routeCnt-1)!=routePt){
							setLED(2);
							flag = 0;
							UARTprintf("out of range \n");
							f_close(&g_sFileObject2);
							routeCnt=0;
							buzzerState(1);
							break;

						}else{
							setLED(0);
							flag = 0;
							tracking = 0;

							f_close(&g_sFileObject2);
							UARTprintf("end point \n");
						}
						f_close(&g_sFileObject2);
						routeCnt=0;
						break;
					}

					f_read(&g_sFileObject2, dataBuffer2, sizeof(dataBuffer2), &usBytesRead);
					dataBuffer2[usBytesRead] = 0;

					HErrVal = pktPointer->lat - cfgPointer->lat;
					VErrVal = pktPointer->lon - cfgPointer->lon;

					errorMargin = 90*10*3;
					//errorMargin = 90*pktPointer->hAcc*3/1000;
					if(routeCnt>=routePt){
						//UARTprintf("%d, %d, %d, %d, %d\n",  ((int32_t)HErrVal<(int32_t)errorMargin), (int32_t)HErrVal>(int32_t)(-1*errorMargin), ((int32_t)VErrVal<(int32_t)errorMargin),((int32_t)VErrVal>(int32_t)(-1*errorMargin)), errorMargin);
						if(((int32_t)HErrVal<(int32_t)errorMargin)&&(int32_t)HErrVal>(int32_t)(-1*errorMargin) && ((int32_t)VErrVal<(int32_t)errorMargin)&&((int32_t)VErrVal>(int32_t)(-1*errorMargin))){
							lastHErrVal=pktPointer->lat - lastLat;
							lastVErrVal=pktPointer->lon - lastLon;
							if(((int32_t)lastHErrVal<(int32_t)errorMargin)&&(int32_t)lastHErrVal>(int32_t)(-1*errorMargin) && ((int32_t)lastVErrVal<(int32_t)errorMargin)&&((int32_t)lastVErrVal>(int32_t)(-1*errorMargin))){
								if((routePt>=(routeNum-3))&&(routePt<=routeNum)){
									setLED(0);
									flag = 0;
									tracking = 0;
									f_close(&g_sFileObject2);
									break;
								}else{
									routePt = routeCnt;
									if(cfgPointer->high){
										mode=2;
									}else{
										mode=0;
									}
									flag = 1;
									f_close(&g_sFileObject2);
									routeCnt=0;
									buzzerState(0);
									setLED(1);
									break;
								}

							}else{
								routePt = routeCnt;
								if(cfgPointer->high){
									mode=2;
								}else{
									mode=0;
								}
								flag = 1;
								f_close(&g_sFileObject2);
								routeCnt=0;
								buzzerState(0);
								setLED(1);
								break;
							}
						}

					}


					routeCnt++;

	//				UARTprintf("Bytes Read: %d\n", usBytesRead);

				}
				doTrack=0;
			}
		}

		//UARTprintf("%d\n", xTaskGetTickCount());
//		if(mode==0){
//			vTaskDelayUntil(&xLastWakeTime, (60*1000/portTICK_RATE_MS));
//		}else{
//			vTaskDelayUntil(&xLastWakeTime, (1000/portTICK_RATE_MS));
//		}
		//UARTprintf("%d, %d, %d\n",  cfgPointer->lat, cfgPointer->lon, errorMargin);

		LEDCount++;
		if(sleeping){
			vTaskDelay(86);
		}else{
			vTaskDelay(20);
		}

//		vTaskDelayUntil(&xLastWakeTime, (1000/portTICK_RATE_MS));
	}
}
/*
void vGPS_Task(void *pvParameters) {
	uint8_t dataBuff[80];


	sendGPSPowMode();
	for(;;) {
		getGPSData(dataBuff);
		xQueueSendToBack(xSdDataQueue, dataBuff, 0);
		vTaskDelay(1000);
	}
}

*/
