#include "ENGG4810.h"


/* Stop with dying message */
void die (FRESULT rc)
{
	uint32_t colour[3] = {0x8000,0,0};
//    RGBInit(1);
//    RGBIntensitySet(0.3f);

	UARTprintf("Failed with rc=%u.\n", rc);
	RGBColorSet(colour);
	RGBEnable();
    for (;;) ;
}
