//*****************************************************************************
//
// project0.c - Example to demonstrate minimal TivaWare setup
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 2.1.0.12573 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_i2c.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"


//*****************************************************************************
//
// Define coefficients for LUX calc
//
//*****************************************************************************
#define LUX_SCALE 14
#define RATIO_SCALE 9
#define CH_SCALE 10
#define CHSCALE_TINT0 0x7517
#define CHSCALE_TINT1 0x0fe7
#define K1T 0x0040 // 0.125 * 2^RATIO_SCALE
#define B1T 0x01f2 // 0.0304 * 2^LUX_SCALE
#define M1T 0x01be // 0.0272 * 2^LUX_SCALE
#define K2T 0x0080 // 0.250 * 2^RATIO_SCALE
#define B2T 0x0214 // 0.0325 * 2^LUX_SCALE
#define M2T 0x02d1 // 0.0440 * 2^LUX_SCALE
#define K3T 0x00c0 // 0.375 * 2^RATIO_SCALE
#define B3T 0x023f // 0.0351 * 2^LUX_SCALE
#define M3T 0x037b // 0.0544 * 2^LUX_SCALE
#define K4T 0x0100 // 0.50 * 2^RATIO_SCALE
#define B4T 0x0270 // 0.0381 * 2^LUX_SCALE
#define M4T 0x03fe // 0.0624 * 2^LUX_SCALE
#define K5T 0x0138 // 0.61 * 2^RATIO_SCALE
#define B5T 0x016f // 0.0224 * 2^LUX_SCALE
#define M5T 0x01fc // 0.0310 * 2^LUX_SCALE
#define K6T 0x019a // 0.80 * 2^RATIO_SCALE
#define B6T 0x00d2 // 0.0128 * 2^LUX_SCALE
#define M6T 0x00fb // 0.0153 * 2^LUX_SCALE
#define K7T 0x029a // 1.3 * 2^RATIO_SCALE
#define B7T 0x0018 // 0.00146 * 2^LUX_SCALE
#define M7T 0x0012 // 0.00112 * 2^LUX_SCALE
#define K8T 0x029a // 1.3 * 2^RATIO_SCALE
#define B8T 0x0000 // 0.000 * 2^LUX_SCALE
#define M8T 0x0000 // 0.000 * 2^LUX_SCALE

//CS Package Coefficients
#define K1C 0x0043 // 0.130 * 2^RATIO_SCALE
#define B1C 0x0204 // 0.0315 * 2^LUX_SCALE
#define M1C 0x01ad // 0.0262 * 2^LUX_SCALE
#define K2C 0x0085 // 0.260 * 2^RATIO_SCALE
#define B2C 0x0228 // 0.0337 * 2^LUX_SCALE
#define M2C 0x02c1 // 0.0430 * 2^LUX_SCALE
#define K3C 0x00c8 // 0.390 * 2^RATIO_SCALE
#define B3C 0x0253 // 0.0363 * 2^LUX_SCALE
#define M3C 0x0363 // 0.0529 * 2^LUX_SCALE
#define K4C 0x010a // 0.520 * 2^RATIO_SCALE
#define B4C 0x0282 // 0.0392 * 2^LUX_SCALE
#define M4C 0x03df // 0.0605 * 2^LUX_SCALE
#define K5C 0x014d // 0.65 * 2^RATIO_SCALE
#define B5C 0x0177 // 0.0229 * 2^LUX_SCALE
#define M5C 0x01dd // 0.0291 * 2^LUX_SCALE
#define K6C 0x019a // 0.80 * 2^RATIO_SCALE
#define B6C 0x0101 // 0.0157 * 2^LUX_SCALE
#define M6C 0x0127 // 0.0180 * 2^LUX_SCALE
#define K7C 0x029a // 1.3 * 2^RATIO_SCALE
#define B7C 0x0037 // 0.00338 * 2^LUX_SCALE
#define M7C 0x002b // 0.00260 * 2^LUX_SCALE
#define K8C 0x029a // 1.3 * 2^RATIO_SCALE
#define B8C 0x0000 // 0.000 * 2^LUX_SCALE
#define M8C 0x0000 // 0.000 * 2^LUX_SCALE
//*****************************************************************************
//
//slave address and data transmit
//
//*****************************************************************************


#define SLAVE_ADDRESS 	0x29 //ADDR pin is selected to ground
#define NUM_I2C_DATA 	15

unsigned int CalculateLux(unsigned int iGain, unsigned int tInt, unsigned int ch0,
unsigned int ch1, int iType);
//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

//*****************************************************************************
//
// Main 'C' Language entry point.  Toggle an LED using TivaWare.
// See http://www.ti.com/tm4c123g-launchpad/project0 for more information and
// tutorial videos.
//
//*****************************************************************************
int
main(void)
{
	unsigned int byte1; //byte 1
	unsigned int byte2; //byte 2
	unsigned int channel0;
	unsigned int channel1;
	unsigned int lux;


	//set the clock
	 SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
	                   SYSCTL_XTAL_16MHZ);

	 //enable i2c and pin B3 and B2
	 SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
	 SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	 GPIOPinConfigure(GPIO_PB2_I2C0SCL);
	 GPIOPinConfigure(GPIO_PB3_I2C0SDA);

	 //select i2c
	 GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
	 GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

	 //Initialise the master
	 I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false);


	 // I2CSlaveEnable(I2C0_BASE);
	 //I2CSlaveInit(I2C0_BASE, SLAVE_ADDRESS);

	 //need to initialise light sensor, set to write
	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false);
	 I2CMasterDataPut(I2C0_BASE,0x03); //power up
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_SINGLE_SEND);
	 while(I2CMasterBusy(I2C0_BASE));

	 while(1){

	 //, write command for channel 0 select
	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false); //slave address may be 0x29 or 0x49 depend
	 I2CMasterDataPut(I2C0_BASE,0xAC);
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_SINGLE_SEND); //command channel
	 while(I2CMasterBusy(I2C0_BASE));

	 //read in channel 0
	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,true);
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_START);
	 while(I2CMasterBusy(I2C0_BASE));
	 byte1 = I2CMasterDataGet(I2C0_BASE); //read into byte 1
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //receive next byte
	 while(I2CMasterBusy(I2C0_BASE)); //wait until end
	 byte2 = I2CMasterBusy(I2C0_BASE); //read next byte
	 //convert to channel0 read
	 channel0=256 * byte2 + byte1;

	 // write command for channel 1 select
	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,false); //slave address may be 0x29 or 0x49 depend
	 I2CMasterDataPut(I2C0_BASE,0xAE);
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_SINGLE_SEND); //command channel
	 while(I2CMasterBusy(I2C0_BASE));

	 //read in channel 1
	 I2CMasterSlaveAddrSet(I2C0_BASE,SLAVE_ADDRESS,true);
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_START);
	 while(I2CMasterBusy(I2C0_BASE));
	 byte1 = I2CMasterDataGet(I2C0_BASE); //read into byte 1
	 I2CMasterControl(I2C0_BASE,I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //receive next byte
	 while(I2CMasterBusy(I2C0_BASE)); //wait until end
	 byte2 = I2CMasterBusy(I2C0_BASE); //read next byte
	 //convert to channel0 read
	 channel1=256 * byte2 + byte1;

	 //calculate lux
	 lux = CalculateLux(1,2,channel0,channel1, 0);
	 }
}
unsigned int CalculateLux(unsigned int iGain, unsigned int tInt, unsigned int ch0,
unsigned int ch1, int iType) {
	unsigned long chScale;
	unsigned long channel1;
	unsigned long channel0;
	switch (tInt)
	{
	case 0: // 13.7 msec
	chScale = CHSCALE_TINT0;
	break;
	case 1: // 101 msec
	chScale = CHSCALE_TINT1;
	break;
	default: // assume no scaling
	chScale = (1 << CH_SCALE);
	break;
	}
	// scale if gain is NOT 16X
	if (!iGain) chScale = chScale << 4; // scale 1X to 16X
	// scale the channel values
	channel0 = (ch0 * chScale) >> CH_SCALE;
	channel1 = (ch1 * chScale) >> CH_SCALE;
	// find the ratio of the channel values (Channel1/Channel0)
	// protect against divide by zero
	unsigned long ratio1 = 0;
	if (channel0 != 0) ratio1 = (channel1 << (RATIO_SCALE+1)) / channel0;
	// round the ratio value
	unsigned long ratio = (ratio1 + 1) >> 1;
	// is ratio <= eachBreak ?
	unsigned int b, m;
	switch (iType)
	{
	case 0: // T package
	if ((ratio >= 0) && (ratio <= K1T))
	{b=B1T; m=M1T;}
	else if (ratio <= K2T)
	{b=B2T; m=M2T;}
	else if (ratio <= K3T)
	{b=B3T; m=M3T;}
	else if (ratio <= K4T)
	{b=B4T; m=M4T;}
	else if (ratio <= K5T)
	{b=B5T; m=M5T;}
	else if (ratio <= K6T)
	{b=B6T; m=M6T;}
	else if (ratio <= K7T)
	{b=B7T; m=M7T;}
	else if (ratio > K8T)
	{b=B8T; m=M8T;}
	break;
	case 1:// CS package
	if ((ratio >= 0) && (ratio <= K1C))
	{b=B1C; m=M1C;}
	else if (ratio <= K2C)
	{b=B2C; m=M2C;}
	else if (ratio <= K3C)
	{b=B3C; m=M3C;}
	else if (ratio <= K4C)
	{b=B4C; m=M4C;}
	else if (ratio <= K5C)
	{b=B5C; m=M5C;}
	else if (ratio <= K6C)
	{b=B6C; m=M6C;}
	else if (ratio <= K7C)
	{b=B7C; m=M7C;}
	else if (ratio > K8C)
	{b=B8C; m=M8C;}
	break;
	}
	unsigned long temp;
	temp = (channel0*b) - (channel1*m);
	// do not allow negative lux value
	if (temp<0) {
		temp = 0;
	}
	temp +=(1<<(LUX_SCALE-1));
	unsigned long lux = temp>>LUX_SCALE;
	return lux;
}

